<?php
session_name('hydrid');
session_start();
require '../../../connect.php';

require '../../../config.php';

require '../../../backend/user/auth/userIsLoggedIn.php';

// Makes sure the person actually has a character set
if ($_SESSION['on_duty'] === "Dispatch") {
    $unit = strip_tags($_GET['unit']);
    $district = strip_tags($_GET['district']);

    $sql = "UPDATE on_duty SET district=? WHERE id=?";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$district, $unit]);
}
