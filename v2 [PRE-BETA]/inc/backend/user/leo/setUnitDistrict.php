<?php
session_name('hydrid');
session_start();
require '../../../connect.php';
require '../../../config.php';
require '../../../backend/user/auth/userIsLoggedIn.php';

// Makes sure the person actually has a character set
if (!isset($_SESSION['identity_name'])) {
  header('Location: ../../../../' . $url['leo'] . '?v=nosession');
  exit();
}

// 
$district = strip_tags($_GET['district']);
$stmt2 = $pdo->prepare("UPDATE `on_duty` SET `district`=:district WHERE `name`=:name");
$stmt2->bindValue(':district', $district);
$stmt2->bindValue(':name', $_SESSION['identity_name']);
$result = $stmt2->execute();

?>