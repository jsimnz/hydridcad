<?php
session_name('hydrid');
session_start();
require '../../../connect.php';

require '../../../config.php';

require '../../../backend/user/auth/userIsLoggedIn.php';

// Makes sure the person actually has a character set

if (!isset($_SESSION['on_duty'])) {
	header('Location: ../../../../' . $url['leo'] . '?v=nosession');
	exit();
}

// check if there is an active panic button for this officer, and archive it
$sql = "SELECT * FROM 911calls WHERE call_status = 'PRIORITY' AND call_description ='" . 'PANIC BUTTON HAS BEEN PUSHED ' . $_SESSION['identity_name'] . "'";
$stmt = $pdo->prepare($sql);

$result = $stmt->execute();
if ($result) {
  $calls = $stmt->fetchAll(PDO::FETCH_ASSOC);
  if (sizeof($calls) > 0) {
    foreach($calls as $call) {
      $sqlArchive = "UPDATE 911calls SET call_status = 'Archive', call_isPriority='false' WHERE call_id ='" . $call["call_id"] . "'";
      $stmtArchive = $pdo->prepare($sqlArchive);
      $resultArchive = $stmtArchive->execute();
      if ($resultArchive) {
        echo "UPDATED 911 PANIC CALL";
        return;
      }
    }
  }
}

//otherwise create a new one

$sql          = "INSERT INTO 911calls (call_description, call_location, call_postal, call_timestamp, call_isPriority, call_status) VALUES (
  :call_description,
  :call_location,
  :call_postal,
  :call_timestamp,
  :call_isPriority,
  :call_status
  )";
$stmt         = $pdo->prepare($sql);
if (!$stmt) {
  echo "INVALID SQL";
  return;
} else {
  echo "OFFICER PANIC VALID SQL";
}
$stmt->bindValue(':call_description', 'PANIC BUTTON HAS BEEN PUSHED ' . $_SESSION['identity_name']);
$stmt->bindValue(':call_location', 'N/A');
$stmt->bindValue(':call_postal', '0');
$stmt->bindValue(':call_timestamp', $us_date . ' ' . $time);
$stmt->bindValue(':call_isPriority', 'true');
$stmt->bindValue(':call_status', 'PRIORITY');
$result = $stmt->execute();
if ($result) {
  if ($settings['discord_alerts'] === 'true') {
  discordAlert('**NEW 911 CALL**
  **Description:** '. $call_description .'
  **Location:** '. $call_location .' / '. $call_crossstreat .' / '. $call_postal .'
  **Called On:** '. $datetime .'
    - **Hydrid CAD System**');
  }
  echo "OFFICER PANIC SET\n";
} else {
  echo "OFFICER PANIC FAILED\n";
  echo print_r($stmt->errorInfo(), true);
}
